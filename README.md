# NodeGen
Mix HTML/CSS and Javascript at a glance! 

[TOC]

NodeGen is a base framework to write your own web app components. It's paradigm, already seen in other framework, is to write your HTML 
parts into a Javascript syntax :
```javascript
with(html){
	
	body(
		div({class: "bookList"},
			span("Choose from", bookList.childNodes.length, "book(s) available :"),
			ul(
	        	div({class:"book"},
					img({src:"./pics/Javascript is fantastic.jpg"}),
					span({class:"title"}, "Javascript is fantastic")
				),
				div({class:"book"},
					img({src:"./pics/Javascript for dummies.jpg"}),
					span({class:"title"}, "Javascript for dummies")
				),
				div({class:"book"},
					img({src:"./pics/Why you should eat MVC at breakfast.jpg"}),
					span({class:"title"}, "Why you should eat MVC at breakfast")
				)
			)
		)
	)
	
}	
```

Ok so you'll say that the advantage writing HTML this way is not so clear !

Javascript offer a loooooot more syntax way to write thing, as HTML is just a declaration language but very human readable.

The advantage of the mix offers HTML easyness to JS creativity and JS flexibility to the HTML straight declarations.

## HTML in JS

Each HTML tag are defined as a js function wich returns the DOMElement object :
```javascript
// Generate DOMElements with node generator functions (a NodeGen)
var myDiv = div();
myDiv instanceof HTMLDivElement; // a native HTMLElement is returned, ready to be append to another
```
```html
<div></div>
```

Every arguments passed to a NodeGen are treated according to type:



### Arguments as childs

If an argument is a DOMElement, a string or a number, then it's added as child or TextNode respectively:
```javascript
var myDiv = div(
            	span(),										// span is also a NodeGen so returns a DOMElement
            	"Text ", 42,
            	document.createElement('h1'),				// Works with Native way created DOMElements
            	h2("Title")									// The string here is added as a child of the <h2> Node
            )
```
```html
<div>
	<span></span>
	Text 42
	<h1><h1>
	<h2>Title<h2>
</div>
```

A way more handy that traditionnal JS way:
```javascript
var myDiv = document.createELement('div');
myDiv.innerHTML = '<span></span>\
	Text 42\
	<h1><h1>\
	<h2>Title<h2>';
```




### Styles and properties

If an argument is an object, then each properties are treated separately and 
are added as style attributes or Node object properties.

 - If the prop name is in this Node's style object, style is added:
```javascript
var myDiv = div({class:"hello", background:"grey"},
				span({color:"red"}, "Text"),
				h2("Title")								
			)
```
```html
<div class="hello" style="background:grey;">
	<span style="color:red;">Text</span>
	<h2>Title<h2>
</div>
```

 - Or if the prop name already exists on Node object it's set the property:
```javascript
var myDiv = div({class:"hello"}, 
				span({color:"red"}, "Text"),			// The children are added,
				h2("Title"),							// but will be erased
				{innerHTML:"Content"} 					// Stupid but works!
			)
```
```html
<div class="hello">Content</div>
```




### Event listeners

If an argument is a function, then it's added as an event listener according to the function name:
```javascript
var myDiv = div(
				span("Text"),
				h2("Title"),
				function click(e){							// The function here is an argument of div() 
					var tag = e.target.tagName;				// so the listener is added to the div
					alert("Hello world! from a " + tag);	// and here, tag = "DIV" when clicked.
				}
			)
```
```html
<div>
	<span>Text</span>
	<h2>Title<h2>
</div>
```




### Unique Tags

Some Nodes can be created only 1 time like head, body or title. They are unique tags.
Corresponding NodeGens ( body() head() title() ) acts a different way than normal ones: it selects
the unique tag against creating a new one. It's usefull to add some to the document's body :
```javascript
body(myDiv);
// is the same as
document.getElementsByTagName('body')[0].appendChild(myDiv);
```

You can write your entire document's body :
```javascript
body(
	div({clas: "hello"},
		span(style: {color: "red"}, "Text"),
		h2("Title")
	)
)
```
```html
<body> 
	already presents nodes
	...
	
	<div class="hello">
		<span style="color: red;">Text</span>
		<h2>Title</h2>
	</div>
</body>
```

 - Unique tags NodeGens acts like a normal NodeGen adding each arguments according to type:
```javascript
title("My Webapp");
// is the same as
document.getElementsByTagName('body')[0].innerHTML = "My Webapp";
// or
document.title = "My Webapp";
```
```javascript
head(
	script("lib/jQuery.js")
)
// or passing an event listener functions
head(
	script("lib/jQuery.js", 
			function load() {							// Magic !
				alert("Hello jQuery !");
			},
			function error() {
				alert("RIP jQuery !");
			}
	)
);
```
```html
<head>
	already presents nodes
	...
	
	<script type="text/javascript" src="lib/jQuery.js"></ script>
</head>
```

 - Setting some global css :
```javascript
head(style("#myPage .Item { display: none; }"));
```
```html
<head>
	already presents nodes
	...
	
	<style>#myPage .Item { display: none; }</style>
</head>
```

Styles can be written in JSON form, NodeGen will take care of browser prefix and units
```javascript
head(
	style({
		"#myPage .Item": {
			display: "block",
			width: 320,
			height: myDiv.offsetHeight / 10,
			transform: "translateX(100px)"
		}
	})
);
```
```html
<head>
	already presents nodes
	...
	
	<style>
	#myPage .Item {
		display: block;
		width: 320;
		height: 42px;
		transform: translateX(100px);
	}
	</style>
</head>
```

 - Unique tag NodeGens also return the tag's Node so it's a convinience to select body or head...
```javascript
var sheets = head().querySelectorAll('link[rel=stylesheet]');
body().childNodes[3].innerHTML = "Number of CSS links :" + sheets.length;
```




## JS in HTML

Now you know how the bases of writing HTML in JS syntax, let's start the fun part!

### Templates

The same way you call a NodeGen to create a Node, you can define a simple function
that returns a Node to create a template :
```javascript
var ColorDiv = function(title, color){
	return div({color: color},
				h1(title),
				hr()
			)
}
var myDiv = div({class:"hello"}, 
				ColorDiv("Hello", 'red'),
				ColorDiv("World", 'green'),
				ColorDiv("of Foo", 'blue')
			)
body(myDiv);
```
```html
<body>
	<div class="hello">
		<div style="color: red;">
			<h1>Hello</h1>
			<hr/>
		</div>
		<div style="color: green;">
			<h1>World</h1>
			<hr/>
		</div>
		<div style="color: blue;">
			<h1>of Foo</h1>
			<hr/>
		</div>
	</div>
</body>
```

```javascript
// Create a book template
var book = function(name)
{
	var book = div({class:"book"},
				   img({src:"./pics/"+name+".jpg"}),
				   span({class:"title"}, name)
			   )
	return book;
}

// Create a book list
var bookList =  ul(
               		book("Javascript is fantastic"),
                	book("Javascript for dummies"),
                	book("Why you should eat MVC at breakfast")
                )

// Add it to the body surrounded by a div container
body(
	div({class: "bookList"},
		span("Choose from", bookList.childNodes.length, "book(s) available :"),
		bookList
	)
)
```




### Naming nodes

The magic is that with javascript you can write variable assignment in 
in several part of code, including arguments:

console.log(bar = 42, 'hello');

bar is set in current context and 42 is assigned, the assignation return 42 also,
so 42 is passed as 1st argument.

With this in mind you can easily save special Nodes reference while writing your DOM:
```javascript
var myDiv = div({class:"hello"}, 
				span({color:"red"}, "Text"),			
titleH2 =		h2("Title")
			)

body(myDiv)
```
```html
<div class="hello">
	<span color="red">Text</span>
	<h2>Title</h2>
</div>
```
and then later
```javascript
titleH2.inerHTML = "A new title !!";
```
```html
<div class="hello">
	<span color="red">Text</span>
	<h2>A new title !!</h2>
</div>
```

We will see later how powerfull this syntax is...

/!\ Note: You can't write nested variable with this syntax:
```javascript
var myDiv = div({class:"hello"}, 
				span({color:"red"}, "Text"),			
myDiv.title =	h2("Title")
			)
```

Because when assigning h2() result to a var (here: myDiv.title) append before 
passing it to main div(), so myDiv is not yet assigned.
The exection order is:
 1 	{class:"hello"}
 2 	{color:"red"}
 3 	"Text"
 4 	span(<2>, <3>)
 5 	"Title"
 6 	h2(<5>)
 7 	myDiv.title =	<6>						// Error here : myDiv is not defined
 8 	var myDiv = div(<1>, <4>, <7>);




## Get organised !

A best practice is to create a namespace to host your components to avoid poluting window object.
All NodeGens creating HTML 5 elements are created in window global context. Except if you included NodeGen.js script tag
with attribute html_namespace="true" : This time all native Node generetors are created into a "html" namespace object.

The problem is that it complexify the writing of complex DOM object like:
```javascript
var myDiv = html.div(
				html.span("Text"),
				html.h2("Title")
			)
```
So the trick is to encapsulate the portion of code in a with(namespace){} statement,
where namespace is the object where to search a NodeGen :
```javascript
with(html){
	var myDiv = div(
					span("Text"),
					h2("Title")
				)
}
```




### Create your own namespace to sort your components

```javascript
var ns = {};

// Create another simple function that return a DOMElement as component
ns.MoreButton = function(text)
{
	with(html){
	return button({class:"magic-button"},
			img({src:"./img/more.png"}),
			span(text)
		)
	}
}

// Create other components
ns.Wonderfull = function(text)
{
	with(html){
		return li(span(text), ns.MoreButton("ADD"))
	}
}

// And use it
with(html){
				body(
					div({width: 100},
						div(
							"Add a wonderfull component",
btn = 						ns.MoreButton("here")
						),
container =				div({id: "container"})
					)
				)
}
btn.onclick = function()
{
	container.appendChild(ns.Wonderfull("Hello I'm Wonderfull num " + (container.childNodes.length + 1)))
}
```

## Create NodeGens

You may want gain NodeGen's way of treating arguments, to do so you can create a new NodeGen by simply calling 
`new NodeGen('tagName', controler);` where controler is a function that is executed on the created node.
The `this` keyword correspond to the node being created.

```javascript
ns.Loading = new NodeGen('div', function()
{
	with(html){
			head(
				style('ns.Loading', {							// Set some global styles for this NodeGen if don't exists
					".loading-indicator": {
						display: 'inline-block',
						height: 50
					},
					".loading-indicator div": {
						background: 'red'
					},
				})
			)
			NodeGen.$(this)(									// Create a unique tag NodeGen with 'this' Node to add some child
	el =		div({height:'100%'})							// Create the indicator div and save reference
			)

	}
	this.className = "loading-indicator";

	var that = this;
	this.setPercent = function(percent)
	{
		el.style.width = that.offsetWith * percent + 'px';		// Set the div width
	}
	this.getPercent = function(percent)
	{
		return 1 / (that.offsetWidth / el.offsetWith);			// Return div percent width
	}
});

with(html){
			body(
indicator =		ns.Loading({width: 200})
			)
}

var loop = setInterval(function()
	{
		var percent = indicator.getPercent()
		indicator.setPercent(percent += 0.1);
		if(percent > 1)
			clearInterval(loop);
	}, 500);

```



A more complicated exemple :

```javascript

ns.Component = new NodeGen('div', function(args)
{
	// Create elements
	with(NodeGen){with(html){
						$(this)(											// Select the node being created and add
							div({class:"Component"},						// a div and add an attribute: class
								div("Messages (", 							// and a div (and add a text node
	numberSpan = 					span({color: args[0]}, 0), 					// and a span (with some style and text) saved in a var: messageNumber 
									")"											// and some text again
								),												// and stop adding into div)
	content = 					div()										// adds another named div in div.Component
							)
						)
	}}

	// Getter / setter with Bindable
	// this.prop = Bindable( { type, get, set } ); returns a function
	// >>> to get the value: this.prop() , to set : this.prop(42) (works if type is correct)
	// To listen value change: this.prop.onChange(function(){})
	this.messageNumber = Bindable({type: Number,
								set: function(v){numberSpan.innerHTML = v;},
								get: function(){return Number(numberSpan.innerHTML);}
							});

	// Override
	var _appendChild = this.appendChild.bind(this), 	// Save the overrided to be able to still use it
		self = this;
	this.appendChild = function(child)
	{
		content.appendChild(child);						// Here we redirect children into the content div
	};
});


NodeGen.ready(function(){ 					// DOMContentLoaded event shortcut
	with(html){
							head(
								style({
									"#content ul li": {
										float: "left",
										display: "block",
										background: "#A60",
										width: 100,
										"margin-left": "10em",
										height: 100
									}
								})
							);
							body(
								ns.Component('red'),
		container =				div({id:"content", style: css({height: 100, background: "grey"})},
		list =						ul(),
									function mouseover(e){ this.style.background = "#F60"; }, 
									function mouseout(e){ this.style.background = "grey"; }, 
									function click(e){
										with(html){
											$(container.list)(
												ns.MyItem("Photo "+ (container.list.childNodes.length + 1))
											);
										}
										this.style.height = parseInt(this.style.height) + 25 + "px";
									}
								)
							);
		// Adds 3 first list items
		var childs = [1,2];
		for(var i=0; i < childs.length; i++)
			childs[i] = ns.MyItem("Photo " + childs[i]);
		// for(var i=0; childs[i++];)
		// 	childs[i] = ns.MyItem("Photo "+i);
		
		$(container.list).apply(this, childs);
		
		$(container.list)(
			ns.MyItem("Photo 3"),
			ns.MyItem("Photo 4"),
			ns.MyItem("Photo 5")
		)
	}
});

```















All Modern Javascript frameworks implements MVC design patterns. I tried to explore another way of thinking HTML components and against separating the view, model and controler parts, the philosophy here is to group all the MVC of a component in the same context (file), and so be able to write less.
