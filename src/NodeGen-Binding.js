(function(){
/**
 * NodeGen framework
 * Mix HTML and Javascript at glance.
 * 
 * @TODO Faire en sorte que les function GenNode génerée puisse être utilisé en controler
 * @TODO System de binding automatique à la Flex "La page fait {document.offsetWith}px de largeur"
 * @TODO Ajouter des eventListener pour les propriété qui commencent par 'on' d'un Object passé en arg
 * @TODO Ajouter des evenement lors d'un style
 * @TODO Implement compileSimpleTag 0 property to select unique tag against creating a new one.
 */

// Function.prototype.getArgument = function(i)
// {
// 	var a=Function.prototype.toString.call(this).match(/\((.*?)\)/)[1].split(',');
// 	for(var n=0,ar;ar=a[n];n++)
// 		a[n]=ar.trim();
// 	return typeof i=='number'?a[i]:a
// }

function Bindable(o)
{
	var isGoodType = function(val){return typeof val != 'undefined';};
	if(typeof o.type != 'undefined')
	{
		// Old IE fix
		o.type.name = o.type.name || (/\s*function\s*(.*?)\(/.exec(Function.prototype.toString.call(o.type)) || [0,''])[1];
		switch(o.type)
		{
			case Boolean:
			case Number:
			case String:
			case Function:
			case Object:
				isGoodType = new Function('val','return typeof val == "'+o.type.name.toLowerCase()+'" || val instanceof '+o.type.name+';'); break;
			default: isGoodType = function(val){return val instanceof o.type;}; break;
		}
	}
	var bindable = function BindableProperty(newVal, willTrigger)
		{
			var a = arguments;
			if(a.length > 0) // Setter
			{
				if(isGoodType(newVal) && typeof o.set == 'function')
				{
					willTrigger = typeof willTrigger == 'undefined' ? true : willTrigger;
					o.set.apply(this, arguments);
					if(willTrigger && typeof arguments.callee.parent != 'undefined')
					{
						bindable.doChange();
						var e = document.createEvent('Event');
						e.initEvent(o.event || 'propertyChange', true, false);
						e.propertyName = "?";
						arguments.callee.parent.dispatchEvent(e);
						// this.dispatchEvent(new BaseEvent(o.event || 'propertyChange'));
					}
				}
			}
			else if(typeof o.get == 'function')
				return o.get.call(this);
		};
	bindable.bindable = true;
	bindable._bound = [];
	bindable.onChange = function(h)
	{
		if(bindable._bound.indexOf(h) == -1)
			bindable._bound.push(h);
	};
	bindable.offChange = function(h)
	{
		if(bindable._bound.indexOf(h) != -1)
			bindable._bound.splice(bindable._bound.indexOf(h),1);
	};
	bindable.doChange = function()
	{
		for(var i=0,h; h=bindable._bound[i++];)		h();
	};
	bindable.toString = function()
	{
		return "[Object.accessor] call it to get, with a value to set. Ex: o.myAccessor() or o.myAccessor(42)";
	};
	return bindable;
}
window.Bindable = Bindable;

var Binding = window.Binding = function(text)
{
	var value, reg = /\{(.*?)\}/g, res, binded = [];
	
	// while(res = reg.exec(text))
	// 	if(typeof res != 'undefined')
	//  		binded.push(res);

	//  for(var i =0, t; t = binnded[i++];)
	 	


	/*if(reg = text.match(/\{(.*)\}/g))
		{
			var event = reg[1],
				prop = reg[2],
				attr2 = attr;
			// TODO: make bridge between lowercase property and case sensitive: ex: innerHTML
			var props = {}, propPath = prop.split('.'), curProp, target = el, realPropPath = [];
			while(propPath.length > 0)
			{
				curProp = propPath.shift();
				for(var n in target) //console.log(n)
					if(curProp == n.toLowerCase())
						curProp = n;
				realPropPath.push(curProp);
				target = target[curProp];
				// console.log(curProp, target);
			}
			
			// console.log(prop, curProp, realPropPath.join('.'), target);
			prop = realPropPath.join('.');
			//console.log(prop);

			// TODO: parse attribute value, find all vars and add listener on propertyChange
			//console.log(attr.value.toString().match(/[A-Za-z_$]+[A-Za-z_$0-9.("')]*//*g));
			document.addEventListener(event, (function(attr, prop)
			{
				return function(event)
				{
					var test = attr.value.toString().match(/\{([^}]*)\}/g);
					if(test.length == 1 && test[0] == attr.value) // Only 1 bracket and no text outside
						eval(	"el."+prop+" = " + attr.value.toString().replace(/\{(.*)\}/g, "($1)")		);
					else
						eval(	"el."+prop+" = \"" + attr.value.toString().replace(/\{([^}]*)\}/g, "\" + ($1) + \"") + "\""	);
					console.log(	"el."+prop+" = \"" + attr.value.toString().replace(/\{([^}]*)\}/g, "\" + ($1) + \"") + "\""	);
				};
			})(attr2, prop));
		}






	return Bindable({type: Number,
						set: function(v){numberSpan.innerHTML = v;},
						get: function(){return Number(numberSpan.innerHTML);}
					});*/
}



// var Test = function(){}
// Test.prototype = {
// 	_value: 0,
// 	value: Bindable({event: 'change',
// 		type: Number,
// 		set: function(v)
// 		{
// 			this._value = v > 10 ? 10 : v;
// 		},
// 		get: function() { return this._value; }
// 	})
// }

// var Test2 = function(){}
// Test2.prototype = {
// 	_otherValue: 0,
// 	otherValue: Bindable({event: 'change',
// 		type: Number,
// 		set: function(v)
// 		{
// 			this._otherValue = v > 10 ? 10 : v;
// 		},
// 		get: function() { return this._otherValue; }
// 	})
// }

// test = new Test;
// test2 = new Test2;
// console.log(test.value());
// test.value(4);
// console.log(test.value());
// test.value.onChange(function()
// 	{
// 		alert("test.value changed to : " + test.value());
// 	});
// test.value(42);

// test2.otherValue = Binding("value is {test.value()} in test et {ca}");
/*
test.value.onChange(function()
{
	test2.otherValue(test.value());
});

test2.otherValue.bindTo(test.value);

*/




// NodeGen.compileBindable = function(func)
// {
// 	var code = Function.prototype.toString.call(func);
// 	var coucouAzee = "coucouAzee haaaaa!!!";
// 	code = code.replace(/\[Bindable(\(.*?\))*\]\s*(.*?);/g, function(found, params, varCode)
// 		{
// 			// console.log(arguments);
// 			var parts = /this\.(.*?)\s*=\s*(.*)/.exec(varCode),
// 				prop = parts[1], v = parts[2],
// 				event = '',
// 				type = '';

// 			// console.log(prop, v);
			
// 			if(params)
// 			{
// 				eval(params);
// 				if(type != '' && typeof type == 'function')
// 				{
// 					type.name = type.name || (/\s*function\s*(.*?)\(/.exec(Function.prototype.toString.call(type)) || [0,''])[1];
// 					type = "type:" + type.name+",";
// 				}
// 				if(event != '' && typeof event == 'string')
// 					event = "this."+prop+".onChange(function(){var e = document.createEvent('Event');e.initEvent('"+event+"', true, false);self.dispatchEvent(e);});";
// 			}
// 			else
// 				event = "this."+prop+".onChange(function(){var e = document.createEvent('Event');e.propertyName='"+prop+"';e.initEvent('propertyChange', true, false);self.dispatchEvent(e);});";
// 			// console.log(event, type);

// 			var newCode = "var _"+prop+" = "+v+""+(event != '' ? ",self=this":"")+"; this."+prop+" = Bindable({"+type+" set:function(v){_"+prop+" = v;}, get:function(){return _"+prop+";}  });"+event;
// 			// console.log(newCode);
// 			return newCode;
// 		});
	
// 	try{
// 		var newFunc = eval("with(func){("+code+")}");
// 	}catch(e){console.log(e)}
	
// 	// console.log(newFunc);
// 	// console.log(typeof newFunc == 'function')
// 	if(typeof newFunc == 'function')
// 		return newFunc;
// 	else
// 		return func;
// }






NodeGen.proxifyContent = function(el, proxy)
{
	var props = ["children",
				"childNodes",
				"childElementCount",
				"firstChild",
				"firstElementChild",
				"innerHTML",
				"lastChild",
				"lastElementChild",
				"innerText"],

		methods = ["appendChild",
				"contains",
				"hasChildNodes",
				"getElementsByTagName",
				"getElementsByClassName",
				"removeChild",
				"insertAdjacentText",
				"insertBefore",
				"insertAdjacentHTML",
				"insertAdjacentElement",
				"replaceChild"],
		origin = {};

	for(var p = 0, prop; prop = props[p++];)
	{
		origin[prop] = el[prop];
		Object.defineProperty(el, prop, {
			get: (function(prop){
					return function()
					{
						return proxy[prop];
					}
			})(prop),
			set: (function(prop){
					return function(v)
					{
						proxy[prop] = v;
						monsystemevent.launch('event'+prop);
					}
			})(prop)
		});
	}
	for(var m = 0, method; method = methods[m++];)
	{
		origin[method] = el[method].bind(el),			// Save the overrided to be able to still use it
			self = this;
		el[method] = (function(method){
						return function(v)
						{
							console.log(el, method, proxy);
							return proxy[method](v);
						}
					})(method)
	}
}
