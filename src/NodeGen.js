(function(){
/**
 * NodeGen framework
 * Mix HTML and Javascript at glance.
 * 
 * @TODO See witch polyfill are needed for IE and include it.
 * @TODO Faire en sorte que les function GenNode génerée puisse être utilisé en controler
 * @TODO System de binding automatique à la Flex "La page fait {document.offsetWith}px de largeur"
 * @TODO Ajouter des eventListener pour les propriété qui commencent par 'on' d'un Object passé en arg
 * @TODO Ajouter des evenement lors d'un style
 * @TODO Implement compileSimpleTag 0 property to select unique tag against creating a new one.
 * @TODO See if script inclusion options eval()uation can be a problem
 */

// Bootstrap: retrieve script inclusion options
var options = window.options = {html_namespace: true},
	thisScript = document.scripts[document.scripts.length - 1],
	ready = false;

for(var i = 0, att; att = thisScript.attributes[i++];)
	if(options.hasOwnProperty(att.name))
		options[att.name] = eval(att.value);
document.addEventListener('DOMContentLoaded', function()
{
	ready = true;
});

// Function.prototype.getArgument = function(i)
// {
// 	var a=Function.prototype.toString.call(this).match(/\((.*?)\)/)[1].split(',');
// 	for(var n=0,ar;ar=a[n];n++)
// 		a[n]=ar.trim();
// 	return typeof i=='number'?a[i]:a
// }

function Bindable(o)
{
	var isGoodType = function(val){return typeof val != 'undefined';};
	if(typeof o.type != 'undefined')
	{
		// Old IE fix
		o.type.name = o.type.name || (/\s*function\s*(.*?)\(/.exec(Function.prototype.toString.call(o.type)) || [0,''])[1];
		switch(o.type)
		{
			case Boolean:
			case Number:
			case String:
			case Function:
			case Object:
				isGoodType = new Function('val','return typeof val == "'+o.type.name.toLowerCase()+'" || val instanceof '+o.type.name+';'); break;
			default: isGoodType = function(val){return val instanceof o.type;}; break;
		}
	}
	var bindable = function BindableProperty(newVal, willTrigger)
		{
			var a = arguments;
			if(a.length > 0) // Setter
			{
				if(isGoodType(newVal) && typeof o.set == 'function')
				{
					willTrigger = typeof willTrigger == 'undefined' ? true : willTrigger;
					o.set.apply(this, arguments);
					if(willTrigger && typeof arguments.callee.parent != 'undefined')
					{
						bindable.doChange();
						var e = document.createEvent('Event');
						e.initEvent(o.event || 'propertyChange', true, false);
						e.propertyName = "?";
						arguments.callee.parent.dispatchEvent(e);
						// this.dispatchEvent(new BaseEvent(o.event || 'propertyChange'));
					}
				}
			}
			else if(typeof o.get == 'function')
				return o.get.call(this);
		};
	bindable.bindable = true;
	bindable._bound = [];
	bindable.onChange = function(h)
	{
		if(bindable._bound.indexOf(h) == -1)
			bindable._bound.push(h);
	};
	bindable.offChange = function(h)
	{
		if(bindable._bound.indexOf(h) != -1)
			bindable._bound.splice(bindable._bound.indexOf(h),1);
	};
	bindable.doChange = function()
	{
		for(var i=0,h; h=bindable._bound[i++];)		h();
	};
	bindable.toString = function()
	{
		return "[Object.accessor] call it to get, with a value to set. Ex: o.myAccessor() or o.myAccessor(42)";
	};
	return bindable;
}
window.Bindable = Bindable;
var Binding = window.Binding = function(text)
{
	var value, reg = /\{(.*?)\}/g, res, binded = [];
	
	// while(res = reg.exec(text))
	// 	if(typeof res != 'undefined')
	//  		binded.push(res);

	//  for(var i =0, t; t = binnded[i++];)
	 	


	/*if(reg = text.match(/\{(.*)\}/g))
		{
			var event = reg[1],
				prop = reg[2],
				attr2 = attr;
			// TODO: make bridge between lowercase property and case sensitive: ex: innerHTML
			var props = {}, propPath = prop.split('.'), curProp, target = el, realPropPath = [];
			while(propPath.length > 0)
			{
				curProp = propPath.shift();
				for(var n in target) //console.log(n)
					if(curProp == n.toLowerCase())
						curProp = n;
				realPropPath.push(curProp);
				target = target[curProp];
				// console.log(curProp, target);
			}
			
			// console.log(prop, curProp, realPropPath.join('.'), target);
			prop = realPropPath.join('.');
			//console.log(prop);

			// TODO: parse attribute value, find all vars and add listener on propertyChange
			//console.log(attr.value.toString().match(/[A-Za-z_$]+[A-Za-z_$0-9.("')]*//*g));
			document.addEventListener(event, (function(attr, prop)
			{
				return function(event)
				{
					var test = attr.value.toString().match(/\{([^}]*)\}/g);
					if(test.length == 1 && test[0] == attr.value) // Only 1 bracket and no text outside
						eval(	"el."+prop+" = " + attr.value.toString().replace(/\{(.*)\}/g, "($1)")		);
					else
						eval(	"el."+prop+" = \"" + attr.value.toString().replace(/\{([^}]*)\}/g, "\" + ($1) + \"") + "\""	);
					console.log(	"el."+prop+" = \"" + attr.value.toString().replace(/\{([^}]*)\}/g, "\" + ($1) + \"") + "\""	);
				};
			})(attr2, prop));
		}






	return Bindable({type: Number,
						set: function(v){numberSpan.innerHTML = v;},
						get: function(){return Number(numberSpan.innerHTML);}
					});*/
}



// var Test = function(){}
// Test.prototype = {
// 	_value: 0,
// 	value: Bindable({event: 'change',
// 		type: Number,
// 		set: function(v)
// 		{
// 			this._value = v > 10 ? 10 : v;
// 		},
// 		get: function() { return this._value; }
// 	})
// }

// var Test2 = function(){}
// Test2.prototype = {
// 	_otherValue: 0,
// 	otherValue: Bindable({event: 'change',
// 		type: Number,
// 		set: function(v)
// 		{
// 			this._otherValue = v > 10 ? 10 : v;
// 		},
// 		get: function() { return this._otherValue; }
// 	})
// }

// test = new Test;
// test2 = new Test2;
// console.log(test.value());
// test.value(4);
// console.log(test.value());
// test.value.onChange(function()
// 	{
// 		alert("test.value changed to : " + test.value());
// 	});
// test.value(42);

// test2.otherValue = Binding("value is {test.value()} in test et {ca}");
/*
test.value.onChange(function()
{
	test2.otherValue(test.value());
});

test2.otherValue.bindTo(test.value);

*/


function NodeGen(tagName, Controler)
{
	var controlers = [].concat(NodeGen.autoMixins).concat(arguments.length > 1 ? 
																Array.prototype.slice.call(arguments, 1) : 
																[]
														),
		isNew = this instanceof arguments.callee,
		isNode = function(o)
		{
			// Let's say it's enough for an HTMLElement
			return typeof o == 'object' && 'nodeType' in o && 'childNodes' in o;
		},
		isUnique = function(s)
		{
			return /html|head|title|body/.test(s);
		},
		// ["name", node]
		isNamed = function(o)
		{
			return typeof o != 'undefined' && o != 'null' && typeof o[0] == 'string' && isNode(o[1]) 	
		},
		funcName = function(f)
		{
			return f.name = (f.name || (/\s*function\s*(.*?)\(/g.exec(f.toString()) || [0,''])[1]);
		};
		
	// console.log(isUnique("div"),isUnique("html"),isUnique("head"),isUnique("title"),isUnique("body"),isUnique("span"),isUnique("ul"));
	// console.log(tagName);
	genNode = function GenNode()
	{
		// Select or create
		var el = isNode(tagName) ? 
					tagName : 
					isUnique(tagName) ? 
						document.getElementsByTagName(tagName)[0] : 
						document.createElement(tagName);
		
		// Augment and inherit
		if((typeof el._initialized == 'undefined' || !el._initialized) && controlers.length > 0)
		{
			for(var c = 0, Controler; Controler = controlers[c++];)
				if(typeof Controler == 'function')
				{
					for(var p in Controler.prototype)
						el[p] = Controler.prototype[p];
					el._initialized = Controler.call(el, arguments) || true;
					// el._initialized = NodeGen.compileBindable(Controler).call(el, arguments) || true;
				}
			// created Event
			var e = document.createEvent('Event');
			e.initEvent('preInitialized', true, false);
			el.dispatchEvent(e);
		}
		
		if(typeof el.$ == 'undefined')
			el.$ = function(sel){return NodeGen(el.querySelector(sel))	};
		
		// Add childs, innerHTML, listeners, properties, styles or attributes
		for(var i = 0; item = arguments[i++];)
		{
			
			if(isNode(item) || isNamed(item))
				isNamed(item) ?
					(isUnique(tagName)?window:el)[item[0]] = el.appendChild(item[1]) :
					el.appendChild(item);


			else if(typeof item == 'string' || typeof item == 'number')
				el.appendChild(document.createTextNode(item.toString()));
			
			else if(typeof item == 'function' && funcName(item) != '')
				(el.addEventListener || el.attachEvent).call(el, item.name, (function(el, handler){
																				return function(e)
																				{
																					if(!handler.disabled)
																					{
																						// Fix the event assignation
																						if (document.all) e = event;
																						handler.call(el, e);
																					}
																				}
																			})(el, item));
			
			else if(typeof item == 'object')
			{
				for(var n in item)
					if(/class/.test(n))
						el.className = item[n];
					else if(typeof item[n] == 'function')
						el[n] = item[n];
					else if(typeof el[n] != 'undefined' && el[n].bindable)
						el[n](item[n]);
					else if(typeof el[n] != 'undefined')
						el[n] = item[n];
					else if(NodeGen.cssProp(n))
						el.style[NodeGen.cssProp(n)] = NodeGen.css((o = {}, o[n] = item[n], o)).match(/:(.*?);/)[1];
					else
					{

						el[n] = Bindable({set: 	(function(n){
													return function(v)
													{
														this["_"+n] = v;
													}
												})(n),
												get: (function(n){
													return function()
													{
														return this["_"+n];
													}
												})(n)
										});
						el[n](item[n], false);
						el[n].onChange((function(n){
											return function()
											{
												var e = document.createEvent('Event');
												e.name = n;
												e.initEvent('propertyChange', true, false);
												el.dispatchEvent(e);
												var e = document.createEvent('Event');
												e.initEvent(n + 'Change', true, false);
												el.dispatchEvent(e);
											}
										})(n));
						// el.setAttribute(n, item[n].toString());
					}
			}
			else
				continue;
		}
		if(!isUnique(tagName))
			return el;
	};

	if(isNew)
		return genNode;
	else
		return new NodeGen(document.querySelector(tagName));
}


// NodeGen.compileBindable = function(func)
// {
// 	var code = Function.prototype.toString.call(func);
// 	var coucouAzee = "coucouAzee haaaaa!!!";
// 	code = code.replace(/\[Bindable(\(.*?\))*\]\s*(.*?);/g, function(found, params, varCode)
// 		{
// 			// console.log(arguments);
// 			var parts = /this\.(.*?)\s*=\s*(.*)/.exec(varCode),
// 				prop = parts[1], v = parts[2],
// 				event = '',
// 				type = '';

// 			// console.log(prop, v);
			
// 			if(params)
// 			{
// 				eval(params);
// 				if(type != '' && typeof type == 'function')
// 				{
// 					type.name = type.name || (/\s*function\s*(.*?)\(/.exec(Function.prototype.toString.call(type)) || [0,''])[1];
// 					type = "type:" + type.name+",";
// 				}
// 				if(event != '' && typeof event == 'string')
// 					event = "this."+prop+".onChange(function(){var e = document.createEvent('Event');e.initEvent('"+event+"', true, false);self.dispatchEvent(e);});";
// 			}
// 			else
// 				event = "this."+prop+".onChange(function(){var e = document.createEvent('Event');e.propertyName='"+prop+"';e.initEvent('propertyChange', true, false);self.dispatchEvent(e);});";
// 			// console.log(event, type);

// 			var newCode = "var _"+prop+" = "+v+""+(event != '' ? ",self=this":"")+"; this."+prop+" = Bindable({"+type+" set:function(v){_"+prop+" = v;}, get:function(){return _"+prop+";}  });"+event;
// 			// console.log(newCode);
// 			return newCode;
// 		});
	
// 	try{
// 		var newFunc = eval("with(func){("+code+")}");
// 	}catch(e){console.log(e)}
	
// 	// console.log(newFunc);
// 	// console.log(typeof newFunc == 'function')
// 	if(typeof newFunc == 'function')
// 		return newFunc;
// 	else
// 		return func;
// }





NodeGen.$ = function(sel){return new NodeGen(sel);	};

//  {'@media screen and (min-width: 200px) and (max-width: 640px)':{'#aze':{height: 200}}}
//  {'#aze':{height: 200}}
//  {height:200}
//X {$color:'#F60',border:'1px solid $color',height:200}

NodeGen.css = function(o)
{
	function parse(o)
	{
		var css = "";
		for(var n in o)
			if(typeof o[n] == 'object')
				css += n + "{" + parse(o[n]) + "}";
			else if(typeof o[n] == 'number' && /width|height|top|left|right|bottom|padding-?.*|margin-?.*/.test(n))
				css += n + ":" + o[n].toString() + "px;";
			else
				css += n + ":" + o[n].toString() + ";";
		return css;
	}
	return parse(o);
}

// check for vendor prefixed names
NodeGen.cssProp = function(name)
{
	var vendors = ["Webkit", "Moz", "O", "ms"], 
		style = document.createElement('a').style;

	if(/src/.test(name))
		return false;
	if(name in style)
		return name;
	
	var capName = name.charAt(0).toUpperCase() + name.slice(1),
		origName = name,
		i = vendors.length;

	while(i--)
	{
		var vendor = vendors[i];
		vendor = vendor.charAt(0).toLowerCase() + vendor.slice(1);
		// console.log(vendor + capName);
		if((name = vendor + capName) in style)
			return name;
	}

	return false;
}


NodeGen.proxifyContent = function(el, proxy)
{
	var props = ["children",
				"childNodes",
				"childElementCount",
				"firstChild",
				"firstElementChild",
				"innerHTML",
				"lastChild",
				"lastElementChild",
				"innerText"],

		methods = ["appendChild",
				"contains",
				"hasChildNodes",
				"getElementsByTagName",
				"getElementsByClassName",
				"removeChild",
				"insertAdjacentText",
				"insertBefore",
				"insertAdjacentHTML",
				"insertAdjacentElement",
				"replaceChild"],
		origin = {};

	for(var p = 0, prop; prop = props[p++];)
	{
		origin[prop] = el[prop];
		Object.defineProperty(el, prop, {
			get: (function(prop){
					return function()
					{
						return proxy[prop];
					}
			})(prop),
			set: (function(prop){
					return function(v)
					{
						proxy[prop] = v;
						monsystemevent.launch('event'+prop);
					}
			})(prop)
		});
	}
	for(var m = 0, method; method = methods[m++];)
	{
		origin[method] = el[method].bind(el),			// Save the overrided to be able to still use it
			self = this;
		el[method] = (function(method){
						return function(v)
						{
							console.log(el, method, proxy);
							return proxy[method](v);
						}
					})(method)
	}
}

NodeGen.import = function()
{
	function exists(name)
	{
		var path = name.split('.'), t = window, current;
		while(path.length)
		{
			current = path.shift();
			if(typeof t[current] == 'undefined')
			{
				// console.log("exists("+name+") = false");
				return [false];
			}
			t = t[current];
		}
		// console.log("exists("+name+") = true");
		return [true, t];
	}

	var test = ready, handlers = [], objects = [];
	// console.log("trying to import " + arguments.length, test);

	for(var i = 0, arg; arg = arguments[i++];)
	{
		if(typeof arg == 'string')
		{
			test &= (here = exists(arg))[0];
			if(!here[0])
			{
				var url = arg.split('.').join('/') + ".js",
					urlCss = arg.split('.').join('/') + ".css";

				var scripts = document.getElementsByTagName('script'),
					alreadyAdded = false;

				// Search for bundle containing this QName
				for(var bundleURL in NodeGen.import.bundles)
					if(NodeGen.import.bundles[bundleURL].indexOf(arg) != -1)
						url = bundleURL;

				NodeGen.load(url);
			}
			else
				objects.push(here[1]);
		}
		else if(typeof arg == 'function')
			handlers.push(arg);
	}
	
	if(test)
	{
		for(var i = 0, handler; handler = handlers[i++];)
			handler.apply(window, objects);
	}
	else
	{
		var args = arguments;
		
		if(typeof args[0] != 'number')
			Array.prototype.unshift.call(args, 0);

		if(args[0]++ < ( Math.round(NodeGen.import.timeout / NodeGen.import.interval) ) - 1)
			setTimeout(function(){ NodeGen.import.apply(window, args); }, NodeGen.import.interval);
	}
}
NodeGen.import.interval = 100; // 100ms
NodeGen.import.timeout = 1000; // 10s
NodeGen.import.bundles = {};
NodeGen.ready = NodeGen.import; // Shortcut: ready(function(){}) , import nothing but still wait for DOMContentLoaded.


NodeGen.Component = function()
{
	var _appendChild = this.appendChild.bind(this), self = this;
	this.appendChild = function(child)
	{
		// console.log("NodeGen.Component.appendChild :", child.nodeName.toLowerCase(), child);
		_appendChild(child);
		var e = document.createEvent('Event');
		e.initEvent('added', true, false);
		child.dispatchEvent(e);
		var e = document.createEvent('Event');
		e.initEvent('childAdded', true, false);
		self.dispatchEvent(e);
	};
}
NodeGen.autoMixins = [NodeGen.Component];



///////////////////////////////////////////////////////////////////////////////
// NODEGEN EXEMPLE : BASE HTML IMPLEMENTATION                                //
///////////////////////////////////////////////////////////////////////////////

/**
 * compileSimpleTags
 * 
 * Create a simple NodeGen for each property in the given object. The object properties values can be
 * 0 for unique tags (Not implemented), 1 for normal tags or a user controler function.
 * @param tags		Object 		An object literal with properties value 0 or 1 or a function.
 * @param ns		Object 		The namespace where to save the NodeGens. Default to window.
 */
NodeGen.compileSimpleTags = function(tags, ns)
{
	for(var tag in tags)
		(ns || window)[tag] = new NodeGen(tag, typeof tags[tag] == 'function' ? tags[tag] : 0[0] );
}

// Create NodeGens for basic HTML tags
var html = window.html = {};
NodeGen.compileSimpleTags({
	// Unique tags
	head:0, title:0, body:0,
	// Simple tags
	div:1, span:1, table:1, td:1, tr:1, ul:1, ol:1, li:1, b:1, img:1, 
	h1:1, h2:1, h3:1, h4:1, h5:1, h6:1, br:1,
	form:1, button:1, textarea:1, 
	// Special tags
	style:function(o)
	{
		return new NodeGen('style')(typeof o == 'object' ? NodeGen.css(o) : o.toString());
	}, 
	img:1, input:1
}, options.html_namespace ? html : 0[0]);







// div = html.div;
html.a = function(href, text)
{
	return new NodeGen('a')({href:href},text);
}
html.script = function(src, type)
{
	type = type || "text/javascript";
	return new NodeGen('script')({src:src,type:type});
}
html.link = function(href)
{
	return new NodeGen('link')({rel: "stylesheet", href: href});
}
html.style = function(o)
{
	return new NodeGen('style')(typeof o == 'object' ? NodeGen.css(o) : o.toString());
}
NodeGen.load = function(src)
{
	var scripts = document.head.getElementsByTagName('script'),
		alreadyAdded = false;

	for(var i = 0, sc; sc = scripts[i++];)
		if(sc.loadedUrl && sc.loadedUrl == src)
			alreadyAdded = true;
	
	if(!alreadyAdded)
	{
		with(html)
		{
			var s = script(src);
			s.loadedUrl = src;
		}
		try{
			document.head.appendChild(s);
		}catch(e){}
		return s;
	}
	return false;
}

window.NodeGen = NodeGen;
//End
})();